import main
import unittest

class FlaskTest(unittest.TestCase):
    def setUp(self):
        self.app = main.app.test_client()

    def tearDown(self):
        pass

    def test_index(self):
        ret = self.app.get("/")
        assert b"Hello world" in ret.data

    def test_miaou(self):
        ret = self.app.get("/miaou/42").get_json()
        self.assertEqual(84, ret['result'])

    def test_healthcheck(self):
        ret = self.app.get("/healthz")
        self.assertEqual(200, ret.status_code)

