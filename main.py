from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/")
def index():
    return "Hello world"

@app.route("/miaou/<int:number>")
def miaou(number):
    return jsonify(result=number * 2)

@app.route("/healthz")
def healthz():
    return jsonify(status="OK")
